import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PoModule } from '@portinari/portinari-ui';
import { RouterModule, Routes } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';



import { LoginModule } from './Login';
import { TarefasModule } from './Tarefa';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        RouterModule,
        FormsModule,
        PoModule,
        HttpClientModule,
        TarefasModule,
        LoginModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
