import { Injectable } from '@angular/core';
import { PoTableColumn } from '@portinari/portinari-ui';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';


import { Tarefa } from './shared';


@Injectable()
export class TarefaService {
    result;
    urlToJson = 'http://localhost:3000/Itens';

    tarefa: Tarefa;
    erro: any;

    constructor(private http: HttpClient) { }
    

    public carregarColunasServ(): Array<PoTableColumn> {

        return [
            { property: 'id', label: 'ID', type: 'number', visible:false},
            { property: 'tarefa', label: 'Tarefa' },
            { property: 'categoria', type: 'label', width: '15%', labels:
                [
                    { value: '1', color: 'color-05', label: 'Analista' },
                    { value: '2', color: 'color-08', label: 'Usuário' }
                ]
            },
            { property: 'datalimite', label: 'Data Limite',width: '12%', type: 'date'},
            { property: 'status', type: 'label', width: '15%', labels:
                [
                    { value: '1', color: 'color-11', label: 'Aberto' },
                    { value: '2', color: 'color-07', label: 'Vence Hoje' },
                    { value: '3', color: 'color-02', label: 'Atrasada' }
                ]
            },
            { property: 'concluida', width: '10%', type: 'label',labels:
                [
                    { value: '1', color: 'color-02', label: 'Concluida' },
                    { value: '2', color: 'color-07', label: 'Pendente' }
                ]
            }
           
        ];
    }

    public carregarItensServ(): Observable<any> {
        return this.http.get(this.urlToJson);
    }

    public inserirItensServ( payload: any): Observable<any> {
        return this.http.post(this.urlToJson, payload);
    }

    public timeStampServ(input) {        

        var event = Date.parse(input);  
        
        return event
    }
    
    public alterarItensServ(id: string, payload: any): Observable<any> {
        return this.http.put(`${this.urlToJson}/${id}`, payload);
    }
    
    
}



