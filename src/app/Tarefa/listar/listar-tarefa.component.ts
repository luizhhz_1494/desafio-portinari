import { Tarefa } from './../model/tarefa.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { PoModalAction, PoModalComponent, PoSelectOption, PoDialogService, PoNotificationService, PoTableColumn, PoDialogType, PoDialogAlertOptions } from '@portinari/portinari-ui';

import { TarefaService } from '../shared';
import { stringify } from 'querystring';



@Component({
    selector: 'app-listar-tarefa',
    templateUrl: './listar-tarefa.component.html',
    styleUrls: ['./listar-tarefa.component.css'],
    providers: [TarefaService]
})

export class ListarTarefaComponent implements OnInit {

    //Variaveis
    columns: Array<PoTableColumn>;
    items = new Array<Tarefa>();
    statusOptions: Array<PoSelectOption>
    status = '';
    id = '';
    datalimite: string = <any>new Date();
    datahoje: string = <any>new Date();
    tarefa: any;
    categoria: string = '';
    concluida: string = '';
    fechar: PoModalAction = {
        action: () => {
            this.fecharCadastro();
        },
        label: 'Fechar',
        danger: true
    };
    confirmar: PoModalAction = {
        action: () => {
            this.cadastrarTarefa();
        },
        label: 'Confirmar'
    };
    jsonInserir: any;


    //ViewChild
    @ViewChild('optionsForm', { static: true }) form: NgForm;
    @ViewChild(PoModalComponent, { static: true }) poModal: PoModalComponent;

    //Construtor
    constructor(private transportService: TarefaService,
        private poDialog: PoDialogService,
        private poNotificationService: PoNotificationService) { };

    public categoriaOptions: Array<PoSelectOption> = [
            { label: 'Analista', value: '1' },
            { label: 'Usuário', value: '2' },
        ];   
     
    public concluidaOptions: Array<PoSelectOption> = [
            {  value: '1', label: 'Concluida', },
            {  value: '2', label: 'Pendente' }
        ]; 

    //On-Init
    ngOnInit() {
        this.restore();
    }

    
    private carregarItens() {
        this.confirmar.loading = false;
        this.transportService.carregarItensServ().subscribe(response => {
            this.items = response;
            this.verificaAtraso(this.items);
        },
            error => {
                console.log('erro', error);
            });
    }

    public verificaAtraso(items: Array<Tarefa>) {
        var dataItem;
        var dataStr: String;
        var dataHj = new Date().toLocaleDateString('en');


        for (let i = 0; i < items.length; i++) {

            dataItem = new Date(this.items[i].datalimite).toLocaleDateString();
            dataStr = this.items[i].datalimite.toString()
            dataItem = dataStr.substr(5, 2) + '/' + dataStr.substr(8, 2) + '/' + dataStr.substr(0, 4);

            if (this.transportService.timeStampServ(dataItem) > this.transportService.timeStampServ(dataHj)) {
                this.items[i].status = '1';
            } else if (this.transportService.timeStampServ(dataItem) === this.transportService.timeStampServ(dataHj)) {
                this.items[i].status = '2';
            } else {
                this.items[i].status = '3';
            }
        }

    };

    public validaData() {
        var dataHj = new Date().toLocaleDateString('en');
        var dataTmp: String;
        var dataLimiteTmp = this.datalimite;
        var ret: boolean;
        if(dataLimiteTmp != ''){
            dataTmp = dataLimiteTmp.toString()
            dataLimiteTmp = dataTmp.substr(5, 2) + '/' + dataTmp.substr(8, 2) + '/' + dataTmp.substr(0, 4);

            if( this.transportService.timeStampServ(dataLimiteTmp) < this.transportService.timeStampServ(dataHj)) {
                this.datalimite = '';
                this.poDialog.alert({ title: 'Data Inválida', message: 'A data não pode ser menor do que o dia de hoje.' });
                ret = false;
            } else {
                ret = true;
            }
        }else{
            this.poDialog.alert({ title: 'Data Inválida', message: 'A data está em branco.' });
            ret = false;
        }
    return ret
    }
    public validaCategoria(){
        this.categoriaOptions = [ { label: 'Analista', value: '1' }, { label: 'Usuário', value: '2' } ];   
    }

    public validaInput() {
        var ret: Boolean;
        if (this.tarefa == null || this.tarefa == '' ) {
            this.poDialog.alert({ title: 'Tarefa em Branco', message: 'Favor preencher o campo Tarefa.' });
            this.tarefa = 'Nova Tarefa'
            ret = false;
        }
        else {
            ret = true;
        }
    return ret
    }

    public abrirCadastro() {
        this.poModal.open();
        this.restore();
    };

    public fecharCadastro() {
        this.form.reset();
        this.poModal.close();
        this.restore();
    };

    public cadastrarTarefa() {

        if(this.validaData() && this.validaInput()){
            this.confirmar.loading = true;
            this.preencherJson();
            setTimeout(() => {
                this.transportService.inserirItensServ(this.jsonInserir)
                    .subscribe(response => {
                        this.poNotificationService.success("Inserido com sucesso.");
                        this.carregarItens();
                        this.fecharCadastro();
                    },
                        error => {
                            console.log('erro', error);
                        });
            }, 7000);
        }

    }

    public preencherJson() {

        this.jsonInserir =
        {
            tarefa: this.tarefa,
            categoria: this.categoria,
            datalimite: this.datalimite,
            status: '',
            concluida: this.concluida
        }
    }

    public restore() {
        this.columns = this.transportService.carregarColunasServ();
        this.carregarItens();
        this.tarefa = 'Nova Tarefa';

        this.statusOptions = [
                { label: 'Aberto', value: '1' },
                { label: 'Vence Hoje', value: '2' },
                { label: 'Atrasada', value: '3' }
            ];
        this.concluidaOptions = [
            { label: 'Concluida', value: '1' },
            { label: 'Pendente', value: '2' }
        ];
        
    }

    public alterItens(rowItem){
        
        var rowItemPut = rowItem;

        console.log('rowItemPut',rowItemPut.id)
        console.log('concluida',this.concluida)


        this.transportService.alterarItensServ( rowItemPut.id, rowItemPut)
        .subscribe(response => {
            this.poNotificationService.success("Tarefa Concluida !");
            this.carregarItens();
        },  
        error => {
            console.error('erro', error);
        });    
         
    }

    public isUndelivered(row, index: number) {
        return row.concluida !== '1';
    }
   

}
