import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { TarefaService, TarefaConcluidaDirective } from './shared';
import { ListarTarefaComponent } from './listar';
import { PoGridModule,  PoPageModule, PoModalModule , PoTableModule, PoWidgetModule, PoFieldModule,PoInfoModule , PoButtonModule } from '@portinari/portinari-ui';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        HttpClientModule,
        PoGridModule,  PoPageModule,PoModalModule, PoTableModule, PoWidgetModule,PoFieldModule, PoInfoModule, PoButtonModule
    ],
    declarations: [
        ListarTarefaComponent,
        TarefaConcluidaDirective
    ],
    providers: [
        TarefaService
    ]
})
export class TarefasModule { }
