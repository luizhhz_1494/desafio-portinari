import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogarComponent } from './Login/logar/logar.component';

import { ListarTarefaComponent } from './Tarefa/listar/listar-tarefa.component';


import { TarefaRoutes } from './Tarefa';


const routes: Routes = [
  {
    path: 'login',
    component: LogarComponent
  },
  
  {
    path: 'tarefa',
    component: ListarTarefaComponent
  },
  { 
		path: '', 
		redirectTo: '/login', 
		pathMatch: 'full' 
	},
	...TarefaRoutes
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
