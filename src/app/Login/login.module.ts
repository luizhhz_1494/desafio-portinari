import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { LoginService } from './login.service';
import { LogarComponent } from './logar/logar.component'

import { PoGridModule, PoPageModule, PoTableModule, PoWidgetModule, PoFieldModule,PoInfoModule , PoButtonModule } from '@portinari/portinari-ui';
import { PoPageLoginModule, PoModalPasswordRecoveryModule } from '@portinari/portinari-templates';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        HttpClientModule,
        PoGridModule, PoPageModule, PoTableModule, PoWidgetModule,PoFieldModule, PoInfoModule, PoButtonModule,
        PoPageLoginModule,
        PoModalPasswordRecoveryModule,
    ],
    declarations: [
        LogarComponent
    ],
    providers: [
        LoginService
    ]
})
export class LoginModule { }
