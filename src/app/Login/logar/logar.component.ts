import { Component, OnInit } from '@angular/core';

import { PoCheckboxGroupOption, PoSelectOption, PoDialogService,PoNotificationService } from '@portinari/portinari-ui';

import { PoPageLogin, PoPageLoginCustomField, PoPageLoginLiterals } from '@portinari/portinari-templates';

import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import { LoginModel } from '../model/login.model';


@Component({
    selector: 'Logar',
    templateUrl: './logar.component.html',
})
export class LogarComponent implements OnInit {

    background: string;
    contactEmail: string;
    customField: PoPageLoginCustomField;
    customFieldOption: any;
    customFieldOptions: Array<PoSelectOption>;
    customLiterals: PoPageLoginLiterals;
    environment: string;
    exceededAttempts: number;
    secondaryLogo: string;
    literals: string;
    login: string;
    loginPattern: string;
    loginError: string;
    loginErrors: Array<string>;
    logo: string;
    passwordError: string;
    passwordErrors: Array<string>;
    passwordPattern: string;
    productName: string;
    properties: Array<string>;
    recovery: string;
    registerUrl: string;
    loginModel = new Array<LoginModel>();

    public readonly propertiesOptions: Array<PoCheckboxGroupOption> = [
        { value: 'hideRememberUser', label: 'Hide remember user' },
        { value: 'loading', label: 'Loading' }
    ];

    constructor(
        private poDialog: PoDialogService, 
        private service: LoginService, 
        private router: Router,
        private poNotificationService: PoNotificationService) { }


    ngOnInit() {
        this.restore();
    }

    addCustomFieldOption() {
        this.customFieldOptions.push({ label: this.customFieldOption.label, value: this.customFieldOption.value });
        this.customField.options = this.customFieldOptions;
        this.onChangeCustomProperties();

        this.customFieldOption = {};
    }

    addLoginError() {
        this.loginErrors.push(this.loginError);
        this.loginError = 'Sua senha esta incorreta';
    }

    addPasswordError() {
        this.passwordErrors.push(this.passwordError);
        this.passwordError = '';
    }

    changeLiterals() {
        try {
            this.customLiterals = JSON.parse(this.literals);
        } catch {
            this.customLiterals = undefined;
        }
    }

    loginSubmit(formData: PoPageLogin) {

        const validMail = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/;

        if (this.exceededAttempts <= 0) {
            // Validação de formato de e-mail
            if (validMail.exec(formData.login.toString())) {
                this.validLogin(formData.login.toString(), formData.password.toString());
            } else {
                this.poDialog.alert({ title: 'Erro', message: 'Favor digitar um e-mail válido !' });
            }
        }
        return;
    }

    private validLogin(login: string, senha: string) {
        let ret: boolean;

        this.service.servLogin(login, senha).
            subscribe(response => {
                this.loginModel = response;

                if (this.loginModel[0].login == login && this.loginModel[0].senha == senha) {
                    this.router.navigate(['tarefa']); 
                    this.poNotificationService.success("Logado com sucesso.");
                    ret = true;            
                }else {
                    if(this.loginModel[0].login == login){
                        this.poDialog.alert({title: 'Erro', message: 'Senha Inválido.'});
                        ret = false;
                    }else{
                        this.poDialog.alert({title: 'Erro', message: 'Login Inválido.'});
                        ret = false;
                    }                    
                }
            }, error => {
                ret = false;
                console.error('erro', error);
            });
        return ret;
    }

    onChangeCustomProperties() {
        this.customField = Object.assign({}, this.customField);
    }

    restore() {
        this.properties = [];
        this.background = '';
        this.contactEmail = '';
        this.customField = { property: undefined };
        this.customFieldOption = { label: undefined, value: undefined };
        this.customFieldOptions = [];
        this.customLiterals = undefined;
        this.environment = '';
        this.exceededAttempts = 0;
        this.secondaryLogo = undefined;
        this.literals = '';
        this.login = '';
        this.loginPattern = '';
        this.loginError = '';
        this.loginErrors = [];
        this.logo = undefined;
        this.passwordError = '';
        this.passwordErrors = [];
        this.passwordPattern = '';
        this.passwordError = '';
        this.passwordErrors = [];
        this.productName = '';
        this.recovery = '';
        this.registerUrl = '';

    }
   
}

