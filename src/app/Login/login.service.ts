import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { take } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { LoginModel } from "./model/login.model";

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    result;
    login: LoginModel;
    erro: any;

    urlLogin = 'http://localhost:3000/Login';

    constructor(private http: HttpClient) { }

    public servLogin(login: string, password: string): Observable<any> {
        let ret = this.http.get(this.urlLogin);
        return ret
    }
}
